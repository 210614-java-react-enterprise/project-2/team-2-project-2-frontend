// const BASE_URL = "http://localhost:8082";
// const BASE_URL = "http://localhost:8080";
const BASE_URL = "http://ec2-18-119-134-111.us-east-2.compute.amazonaws.com:80"

function sendAjaxRequest(method, url, body, requestHeaders, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    for(let requestHeaderKey in requestHeaders){
        // {"Authorization" : "auth-token", "Content-Type": "application/json"}
        xhr.setRequestHeader(requestHeaderKey, requestHeaders[requestHeaderKey]);
    }
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        //   console.log(xhr);
        if (xhr.status>199 && xhr.status<300) {
          successCallback(xhr);
        } else {
          failureCallback(xhr);
        }
      }
    };
    if(body){
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function sendAjaxGet(url, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("GET", url, null, requestHeaders, successCallback, failureCallback);
}

function sendAjaxPost(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("POST",url, body, requestHeaders, successCallback, failureCallback);
}

function sendAjaxPut(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("PUT",url, body, requestHeaders, successCallback, failureCallback);
}

function sendAjaxDelete(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("DELETE",url, body, requestHeaders, successCallback, failureCallback);
}

function ajaxUpdateBookmark(authToken, bookmarkId, bookmark, successCallback, failureCallback){
    let requestUrl = `${BASE_URL}/bookmarks/${bookmarkId}`;
    let requestHeaders = { 
        "Content-Type": "application/json",
        "Authorization": authToken
    };
    let requestBody = bookmark;
    sendAjaxPut(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
} 

function ajaxDeleteUserBookmark(userId, authToken, bookmarkId, successCallback, failureCallback){
    let requestUrl = `${BASE_URL}/bookmarks/${bookmarkId}`;
    let requestHeaders = {
        "Authorization": authToken,
        "userId": userId
    };
    sendAjaxDelete(requestUrl, false, requestHeaders, successCallback, failureCallback);
} 

function ajaxGetBookmark(bookmarkId, authToken, successCallback, failureCallback){
    let requestUrl = `${BASE_URL}/bookmarks/${bookmarkId}`;
    let requestHeaders = {
        "Authorization": authToken
    };
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
} 

function ajaxGetUserBookmarks(userId, authToken, successCallback, failureCallback){
    let requestUrl = `${BASE_URL}/bookmarks`;
    let requestHeaders = {
        "Authorization": authToken,
        "userId": userId
    };
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
} 

function ajaxPostBookmark(userId, authToken, bookmark, successCallback, failureCallback){
    let requestUrl = `${BASE_URL}/bookmarks`;
    let requestHeaders = {
        "Content-Type": "application/json",
        "Authorization": authToken,
        "userId": userId
    };
    let requestBody = bookmark;
    sendAjaxPost(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
} 

function ajaxGetUser(username, authToken, successCallback, failureCallback){
    let requestUrl = `${BASE_URL}/users/${username}`;
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded", "Access-Control-Expose-Headers":"*"};
    let requestUrl = `${BASE_URL}/login`;
    let requestBody = `username=${username}&password=${password}`;
    sendAjaxPost(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}

function ajaxRegister(username, password, email, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded"};
    let requestUrl = `${BASE_URL}/register`;
    let requestBody = `username=${username}&password=${password}&email=${email}`;
    sendAjaxPost(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}

function ajaxUpdateUser(username, newPassword, email, oldPassword, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded", "Username":username, "Authorization":sessionStorage.getItem("token")};
    let requestUrl = `${BASE_URL}/users/update`;
    let requestBody = `username=${username}&newPassword=${newPassword}&email=${email}&oldPassword=${oldPassword}`;
    sendAjaxPut(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}

function ajaxDeleteUser(username, password, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded", "Username":username, "Authorization":sessionStorage.getItem("token")};
    let requestUrl = `${BASE_URL}/users/delete`;
    let requestBody = `username=${username}&password=${password}`;
    sendAjaxDelete(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}

function ajaxGetAllBookmarks(authToken, successCallback, failureCallback){
    let requestHeaders ={"Authorization": authToken};
    let requestUrl = `${BASE_URL}/bookmarks/public`;
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function ajaxGetGoogleSearch(searchTopic, authToken, successCallback, failureCallback){
    let requestHeaders ={"Authorization": authToken};
    let requestUrl = `${BASE_URL}/googlesearch/${searchTopic}`;
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function test(){
    console.log("test")
}

export {
    ajaxPostBookmark, 
    ajaxLogin, 
    ajaxRegister, 
    ajaxUpdateUser, 
    ajaxDeleteUser, 
    ajaxGetUser, 
    ajaxGetUserBookmarks, 
    ajaxDeleteUserBookmark,
    ajaxGetBookmark,
    ajaxUpdateBookmark,
    ajaxGetAllBookmarks,
    ajaxGetGoogleSearch,
    test
};