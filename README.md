# Constellation

## Description
This project is a full stack web application, which can be used to keep track of the bookmarks, without being concerned about the browser. User will be able to access their bookmarks effortlessly on different browsers. It also uses Google Search API to help users search in the same way as Google search.

## Table Of Contents
* [User stories](#user-stories)
* [Technologies used](#technologies-used)
* [How to use?](#how-to-use)
* [API Github Repo Link](#api-github-repo-link)
* [Contributors](#contributors)

## User Stories
* User can create account in Constellation using username, password and email
* User can login to Constellation using registered username and password
* User cannot access anypages without logging in
* User can view all private bookmarks in the home page
* User can edit(can add rating, share to public so that other users can also access) those private bookmarks
* User can delete bookmarks
* User can filter bookmarked urls typing tag name
* User can bookmark any url with title, url and tag name (in addition user can add rating and make it private or public)
* User can search for a particular topic and is able to bookmark any of those links
* User is able to view all public bookmarks
* User can download all bookmarks as .csv file
* User can update user information
* User will be able to sign-off from the account, which deletes the account, but keeps the user info, so that later if user wants to come back and use, they can
* User is able to logout from the application

## Technologies Used
* HTML
* CSS
* JavaScript
* Bootstrap
* Single page application
* Google Search API
* AJAX
* jQuery
* AWS S3

## How to Use?
* Open  using any browser
* Register using username, password and email
* Login to your account using username and password
* If you already have a url to be bookmarked, then click on "new" inside home page and enter tag, url and title to save
* You can use Edit button and delete button next to each bookmarks to edit bookmark details(share to public, rating etc) and delete
* If you want to search for a particular item and bookmark a link from the search results, use explore page to search and fom the results displayed, click on the bookmarks button next to each result to bookmark
* You can view all public bookmarks (booked by other users as public) in the community tab
* You can download all bookmarks as .csv file using download tab
* You can update user information using update tab
* If you want to sign-off from the account, then use sign-off in the update page
* Once you have finished using the application, you can logout


## API Github Repo Link
[Constellation API Github Repo](https://github.com/elvis-ican/team2-p2-api-local-running)

## Contributors
[Wyatt Goettsch](https://github.com/Sife-ops)

[Elvis Lee](https://github.com/elvis-ican)

[Rensy Aikara](https://github.com/RensyAikara)




