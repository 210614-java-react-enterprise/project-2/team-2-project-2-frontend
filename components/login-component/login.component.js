import { ajaxLogin } from "../../server-requests.js";



document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event){
    event.preventDefault();
    
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;

    ajaxLogin(username, password, loginSuccess, loginFail);
}

function loginSuccess(xhr){
    let token = xhr.getResponseHeader("Authorization");
    let username = xhr.getResponseHeader("Username");
    let userId = xhr.getResponseHeader("UserId");
    sessionStorage.setItem("token", token);
    sessionStorage.setItem("username", username);
    sessionStorage.setItem("userId", userId);
    window.location.href = "#/home";
}

function loginFail(){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;

}

