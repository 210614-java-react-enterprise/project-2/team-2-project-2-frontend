import { ajaxGetUserBookmarks } from "../../server-requests.js";

document.getElementById("download-confirm").addEventListener("click", attemptDownload);
var bookmarks = [];
const token = sessionStorage.getItem("token");
const userId = sessionStorage.getItem("userId");

ajaxGetUserBookmarks(
    userId,
    token,
    renderBookmarks,
    displayFail
);

function displayFail(){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
}

function renderBookmarks(xhr){
    const bookmarksJson = xhr.responseText;
    bookmarks = JSON.parse(bookmarksJson);
    const bookmarksHtml = bookmarks.map((bookmark) =>
    `<tr>
    <td>${bookmark.title}</td>
    <td><a href=${bookmark.url} target="_blank" id="tableurls">${bookmark.url}</a></td>
    <td>${bookmark.tag}</td>
    <td>${bookmark.rating}</td>
    </tr>`
    ).join("");
    document.getElementById("book-list").innerHTML = bookmarksHtml;
}

function attemptDownload(event){
    bookmarks = bookmarks.map(e => e.title + ", " + e.url + ", " + e.tag + ", " + e.rating).join("\n");
    bookmarks = "Title, URL, Tag, Rating\n" + bookmarks;

    let hiddenLink = document.createElement("a");

    let encodedUri = "data:text/csv;charset=utf-8," + encodeURI(bookmarks);
    hiddenLink.setAttribute("href", encodedUri);
    hiddenLink.setAttribute("download", "My_constellation_bookmarks.csv");
    hiddenLink.target = "_blank";

    document.body.appendChild(hiddenLink);
    hiddenLink.click();
}
