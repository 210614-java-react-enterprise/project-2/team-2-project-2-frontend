import { ajaxGetGoogleSearch, ajaxPostBookmark } from "../../server-requests.js";
let buttonElement = document.getElementById("button-element");
document.getElementById("search-message").style.display = "none";
let inputValue = document.getElementById("searchInput");
let addCards = document.getElementById("app-root");
const token = sessionStorage.getItem("token");
const userId = sessionStorage.getItem("userId")
var containerCard = document.createElement("section");
 buttonElement.addEventListener("click", searchApi);
 window.addEventListener("hashchange",searchApi);
 
function searchApi(){
    console.log("search is clicked");
    if(inputValue.value == ""){
        document.getElementById("search-message").textContent = "You need to type something to search!!!!";
        document.getElementById("search-message").style.display = "block";
        
        let resultCont = document.getElementById("resultContainer");
       if(resultCont != null){
        resultCont.innerHTML = "";
       }
    }
    else{
        document.getElementById("search-message").style.display = "none";
        let resultCont = document.getElementById("resultContainer");
        if(resultCont != null){
        resultCont.innerHTML = "";
        document.getElementById("search-message").style.display = "none";
        }

        let query = inputValue.value;
        ajaxGetGoogleSearch(query, token, renderSearchResults, logError)
    }
}

function renderSearchResults(xhr) {
    const itemsArray = JSON.parse(xhr.responseText);
    containerCard.setAttribute("id", "resultContainer");
    addCards.appendChild(containerCard);
    let query = inputValue.value;

    var divResults = document.createElement("h5");
        divResults.setAttribute("class","textSearch");
        divResults.textContent = "Search Results......";
        containerCard.appendChild(divResults);
        
        itemsArray.forEach(element => {
            displayItems(element);
        });
        $(document).on("click", ".buttonMark", function(event){
            ajaxPostBookmark(
                userId,token,
                JSON.stringify({
                    title: $(this).data("title"),
                    url: $(this).data("url"),
                    tag: query,
                    rating: 0,
                    sharing: false
                }), successCallback, failureCallback);

        })
}

function logError(xhr){
    document.getElementById("search-message").textContent = "There was an issue with the request";
    document.getElementById("search-message").style.display = "block";
}

function successCallback(){
    document.getElementById("search-message").textContent = "URL added to bookmark successfully";
    document.getElementById("search-message").style.display = "block";
}

function failureCallback(){
    document.getElementById("search-message").textContent = "Error adding URL";
    document.getElementById("search-message").style.display = "block";
}

function displayItems(element){
    let searchTitle = element.title;
    let searchLink = element.link;
    let searchDetails = element.htmlSnippet;

    var divRow = document.createElement("div");
    divRow.setAttribute("class", "card newCard bg-custom-1");
    containerCard.appendChild(divRow);

    var divColumn = document.createElement("div");
    divRow.appendChild(divColumn);

    var divColumn1 = document.createElement("div");
    divRow.appendChild(divColumn1);

    var divColumn2 = document.createElement("div");
    divRow.appendChild(divColumn2);

    var titleElement = document.createElement("h6");
    titleElement.setAttribute("class", "attributes titleText");
    titleElement.textContent = searchTitle;
    divColumn.appendChild(titleElement);

    var bodyElement = document.createElement("p");
    bodyElement.setAttribute("class", "attributes titlebody");
    bodyElement.innerHTML = searchDetails;
    divColumn1.appendChild(bodyElement);

    var linkElement = document.createElement("a");
    linkElement.setAttribute("class", "attributes");
    linkElement.setAttribute("id", "titleLink");
    linkElement.setAttribute("href", searchLink);
    linkElement.setAttribute("target", "_blank");
    linkElement.textContent = "Click here to view";
    divColumn2.appendChild(linkElement);

    var bookMarkButton = document.createElement("button");
    bookMarkButton.setAttribute("class", "buttonMark");
    bookMarkButton.setAttribute("data-title", searchTitle);
    bookMarkButton.setAttribute("data-url", searchLink);
    bookMarkButton.textContent = "BookMark";
    divColumn2.appendChild(bookMarkButton);
}
