import { 
  ajaxDeleteUserBookmark, 
  ajaxGetBookmark, 
  ajaxGetUserBookmarks, 
  ajaxPostBookmark, 
  ajaxUpdateBookmark
} from "../../server-requests.js";

/**
 * Check token
 */
const token = sessionStorage.getItem("token");
if (token == null) {
    window.location.href = "#/login";
} else {

  /**
   * New bookmark
   */
  document.getElementById("newBookmarkFormButton")
    .addEventListener("click", function(event) {

    renderBookmarkForm("New Bookmark");

    document.getElementById("newBookmarkSaveButton")
      .addEventListener("click", function(event) {

      document.getElementById("newBookmarkSaveButton").disabled = true;
      event.preventDefault();

      ajaxPostBookmark(
        userId,token,
        JSON.stringify({
          title: document.getElementById("newBookmarkTitle").value,
          url: document.getElementById("newBookmarkUrl").value,
          tag: document.getElementById("newBookmarkTag").value,
          rating: document.getElementById("newBookmarkRating").value,
          sharing: document.getElementById("newBookmarkVisibility").value
        }),
        ()=>{
          console.log("something good happened");
          window.location.reload();
        },
        ()=>{console.log("something bad happened")}
      );

    });

    document.getElementById("newBookmarkCancelButton")
      .addEventListener("click", (event)=>{

      document.getElementById("newBookmarkCancelButton").disabled = true;
      event.preventDefault();

      window.location.reload();
    });
  });

  /**
   * Main list
   */
  const userId = sessionStorage.getItem("userId")
  ajaxGetUserBookmarks(
    userId,
    token,
    renderMainList,
    () => {
      console.log("something REALLY bad happened");
    }
  );

  function renderMainList(xhr) {
    let responseText = JSON.parse(xhr.responseText);
    let listItems = responseText.map(generateListItems).join("");

    const mainListTable = document.getElementById("mainListTable");
    mainListTable.innerHTML = listItems;
  
    const editBookmarkButtonNodes = document.querySelectorAll('[id^=editBookmarkButton-');
    const deleteBookmarkButtonNodes = document.querySelectorAll('[id^=deleteBookmarkButton-');

    editBookmarkButtonNodes.forEach(generateEditForm);
    deleteBookmarkButtonNodes.forEach(generateDeleteForm);
  }

  /**
   * Functions for main list.
   */
  let generateListItems = e => {
    return `
      <tr id="bookmarkListItem-${e.id}">
        <td>
          <a href="${e.url}">${e.title}</a>
        </td>
        <td>
          <a href="${e.url}">${e.url}</a>
        </td>
        <td>
          <button id="editBookmarkButton-${e.id}">Edit</button>
          <button id="deleteBookmarkButton-${e.id}">Delete</button>
          <div id="deleteBookmarkFormDiv-${e.id}"></div>
        </td>
      </tr>
    `;
  }

  let generateEditForm = e => {
    let idAttributeString = e.getAttribute("id").toString();
    document.getElementById(idAttributeString)
      .addEventListener("click", () => {

      let idString = idAttributeString.replace(/^.*-/,"");

      ajaxGetBookmark(
        idString, token, (xhr)=>{
          console.log("something good happened")
          let oldBookMark = JSON.parse(xhr.responseText);
          console.log(oldBookMark)
            
          renderBookmarkForm("Edit Bookmark");

          document.getElementById("newBookmarkTitle").value =
            oldBookMark.title;
          document.getElementById("newBookmarkUrl").value =
            oldBookMark.url;
          document.getElementById("newBookmarkTag").value =
            oldBookMark.tag;
          document.getElementById("newBookmarkRating").value =
            oldBookMark.rating;
          document.getElementById("newBookmarkVisibility").value =
            oldBookMark.sharing;

          document.getElementById("newBookmarkSaveButton")
            .addEventListener("click", (event)=>{

            document.getElementById("newBookmarkSaveButton").disabled = true;
            event.preventDefault();

            ajaxUpdateBookmark(
              token, oldBookMark.id,
              JSON.stringify({
                title: document.getElementById("newBookmarkTitle").value,
                url: document.getElementById("newBookmarkUrl").value,
                tag: document.getElementById("newBookmarkTag").value,
                rating: document.getElementById("newBookmarkRating").value,
                sharing: document.getElementById("newBookmarkVisibility").value
              }),
              ()=>{
                console.log("something good happened");
                window.location.reload();
              },
              ()=>{
                console.log("something bad happened")
              }
            );
          });

          document.getElementById("newBookmarkCancelButton")
            .addEventListener("click", (event)=>{

            document.getElementById("newBookmarkCancelButton").disabled = true;
            event.preventDefault();

            window.location.reload();
          });
        },
        ()=>{
          console.log("something bad happened")
        }
      );
    });
  }

  let generateDeleteForm = e =>{
    let idAttributeString = e.getAttribute("id").toString();
    document.getElementById(idAttributeString)
      .addEventListener("click", () => {

      let idString = idAttributeString.replace(/^.*-/,"");
      document.getElementById("deleteBookmarkFormDiv-" + idString)
        .innerHTML = `
          <p>Are you sure?</p>
          <button id="deleteBookmarkYesButton-${idString}">Yes</button>
          <button id="deleteBookmarkNoButton-${idString}">No</button>
      `;

      document.getElementById("deleteBookmarkYesButton-" + idString)
        .addEventListener("click", (event)=>{

        document.getElementById("deleteBookmarkYesButton-" + idString).disabled = true;
        event.preventDefault();

        ajaxDeleteUserBookmark(
          userId,token,idString,
          ()=>{
            console.log("something good happened");
            window.location.reload();
          },
          ()=>{ 
            console.log("something bad happened");
          }
        )
      });

      document.getElementById("deleteBookmarkNoButton-" + idString)
        .addEventListener("click", (event)=>{

        document.getElementById("deleteBookmarkNoButton-" + idString).disabled = true;
        event.preventDefault();

        window.location.reload();
      });
    });
  }

  function renderBookmarkForm(headerText) {
    document.getElementById("mainDiv").innerHTML = `
      <h1>${headerText}</h1>
      <br>
      <input id="newBookmarkTitle" type="text" placeholder="Title">
      <input id="newBookmarkUrl" type="text" placeholder="URL">
      <input id="newBookmarkTag" type="text" placeholder="Tag">
      <br>
      <label for="newBookmarkRating">Rating:</label>
      <select id="newBookmarkRating" name="newBookmarkRating" style="display: block;">
        <option value="3">3</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </select>
      <br>
      <label for="newBookmarkVisibility">Visibility:</label>
      <select id="newBookmarkVisibility" name="newBookmarkVisibility" style="display: block;">
        <option value="false">private</option>
        <option value="true">public</option>
      </select>
      <br>
      <button id="newBookmarkSaveButton">Save</button>
      <button id="newBookmarkCancelButton">Cancel</button>
    `;
  }

  function renderCancelButton() {
    document.getElementById("newBookmarkCancelButton")
      .addEventListener("click", (event)=>{

      document.getElementById("newBookmarkCancelButton").disabled = true;
      event.preventDefault();

      window.location.reload();
    });
  }

} 
