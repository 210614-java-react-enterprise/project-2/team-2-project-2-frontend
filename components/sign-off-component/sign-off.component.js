import { ajaxDeleteUser } from "../../server-requests.js";

document.getElementById("sign-off-form").addEventListener("submit", attemptSignOff);

function attemptSignOff(event){
    event.preventDefault();

    const username = sessionStorage.getItem("username").toString();
    const password = document.getElementById("password").value;
    const confirmPassword = document.getElementById("confirmPassword").value;

    if (password === confirmPassword) {
        if (window.confirm("Are you sure to sign-off?")) {
            ajaxDeleteUser(username, password, deleteSuccess, deleteFail);
        } else {
            window.location.href = "#/home";
        }
    } else {
        deleteFail();
    }
}    

function deleteSuccess(xhr){
    alert("You've been signed off, sorry to see you go!");
    sessionStorage.clear();
    window.location.href = "#/login";
}

function deleteFail(){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
}
