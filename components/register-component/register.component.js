import { ajaxRegister } from "../../server-requests.js";



document.getElementById("register-form").addEventListener("submit", attemptRegister);

function attemptRegister(event){
    event.preventDefault();
    
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    const email = document.getElementById("inputEmail").value;

    ajaxRegister(username, password, email, registerSuccess, registerFail);
}

function registerSuccess(xhr){
    alert("Congratulations, your registration is successful! Please login to proceed.");
    window.location.href = "#/login";
}

function registerFail(){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
}

