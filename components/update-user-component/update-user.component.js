import { ajaxUpdateUser } from "../../server-requests.js";

document.getElementById("update-form").addEventListener("submit", attemptUpdate);

function attemptUpdate(event){
    event.preventDefault();
    
    const username = sessionStorage.getItem("username").toString();
    const newPassword = document.getElementById("newPassword").value;
    const email = document.getElementById("inputEmail").value;
    const oldPassword = document.getElementById("oldPassword").value;

    ajaxUpdateUser(username, newPassword, email, oldPassword, updateSuccess, updateFail);
}

function updateSuccess(xhr){
    alert("Your information was updated.");
    window.location.href = "#/home";
}

function updateFail(){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
}

