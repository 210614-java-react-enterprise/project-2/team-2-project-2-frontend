import { ajaxGetAllBookmarks } from "../../server-requests.js";



window.addEventListener("load",ajaxGetAllBookmarks);
const token = sessionStorage.getItem("token");

// window.onload = function(){
    
    ajaxGetAllBookmarks(token, renderBookmarks, logError);
    
// }

function renderBookmarks(xhr){
    const bookmarksJson = xhr.responseText;
    const bookmarks = JSON.parse(bookmarksJson);
    console.log(bookmarks);
    const bookmarksHtml = bookmarks.map((bookmark) =>
    `<tr>
    <td>${bookmark.title}</td>
    <td><a href=${bookmark.url} target="_blank" id="tableurls">${bookmark.url}</a></td>
    <td>${bookmark.tag}</td>
    <td>${bookmark.rating}</td>
    </tr>`
    ).join("");
    document.getElementById("book-list").innerHTML = bookmarksHtml;
    console.log("successfulll one");
}

function logError(xhr){
    console.log("there was an issue with the request");
}