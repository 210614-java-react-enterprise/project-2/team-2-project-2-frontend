const root = document.getElementById("app-root");
// document.getElementById("login-message").style.display = "none";
window.addEventListener("load", route);
window.addEventListener("hashchange", route);
document.getElementById("logout-link").addEventListener("click", attemptLogout);

function attemptLogout(event){
  if (sessionStorage.getItem("token") == null) {
    window.location.href = "#/login";
  } else{
    if (window.confirm("Are you sure to logout?")) {
      sessionStorage.clear();
      window.location.href = "#/login";
    }
    else{
      window.location.href.reload(true);
    }
    }
}

const routes = [
    {path: "", componentFileName: "login"},
    {path: "#/login", componentFileName: "login"},
    {path: "#/register", componentFileName: "register"}
    ];
    
const routesForUsers = [
    {path: "#/bookmarks", componentFileName: "bookmarks"},
    {path: "#/home", componentFileName: "home"},
    {path: "#/update-user", componentFileName: "update-user"},
    {path: "#/community", componentFileName: "community"},
    {path: "#/download", componentFileName: "download"}
]

// console.log(location);

function route(){
    
    const hashPath = location.hash;
    console.log("hashPath is : "+hashPath);
    let viewName = "";
    let currentRouteValue =  currentRoute(hashPath)

    if (currentRouteValue == undefined){
        currentRouteValue =  currentUserRoute(hashPath);
        if(currentRouteValue == undefined){
            viewName = "login";
            window.location.href = "#/login";
            location.reload();
            // document.getElementById("login-message").style.display = "block";
        }
        else{
            viewName =currentRouteValue.componentFileName ? currentRouteValue.componentFileName : "login";
        }
    }
    else{
        viewName =currentRouteValue.componentFileName ? currentRouteValue.componentFileName : "login";
    }

    console.log("ViewName is: "+viewName);
    renderView(viewName);
}

function currentRoute(hashPath){
    for(let r of routes){
        if(r.path === hashPath){
            console.log("success");
            return r;
        }
    }
    return undefined;
}

function currentUserRoute(hashPath){
    for(let r of routesForUsers){
        if(r.path === hashPath){
            console.log("success");
            if(sessionStorage.getItem("token") != null){
            return r;
            }
        }
    }
    return undefined;
    
}

function renderView(view){
    fetch(`components/${view}-component/${view}.component.html`)
    .then(response=>response.text())
    .then(htmlBody=>{
        root.innerHTML = htmlBody;
        console.log("html body is : "+htmlBody);
        loadScript(view);
    });
}

function loadScript(scriptName){
    let localScript = document.getElementById("dynamic-js");
    console.log(localScript);
    if (localScript){
        localScript.remove();
    }
    localScript = document.createElement("script");
    localScript.id = "dynamic-js";
    localScript.src = `components/${scriptName}-component/${scriptName}.component.js`;
    localScript.type = "module";
    document.body.appendChild(localScript);

}

// function filterBar() {
//     var input, filter, ul, li, a, i, txtValue;
//     input = document.getElementById('filterBarText');
//     filter = input.value.toUpperCase();
//     ul = document.getElementById("mainListUl");
//     li = ul.getElementsByTagName('li');
//     for (i = 0; i < li.length; i++) {
//         a = li[i].getElementsByTagName("a")[0];
//         txtValue = a.textContent || a.innerText;
//         if (txtValue.toUpperCase().indexOf(filter) > -1) {
//         li[i].style.display = "";
//         } else {
//         li[i].style.display = "none";
//         }
//     }
// }

function filter() {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("filterBarText");
    filter = input.value.toUpperCase();
    table = document.getElementById("mainListTable");
    tr = table.getElementsByTagName("tr");
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
        }
    }
}
